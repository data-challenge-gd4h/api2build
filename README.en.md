# Challenge GD4H - API2Build'

<a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) is an initiative led by the Innovation Lab (Ecolab) of the French ministry of ecology.

A challenge has been organised in 2023 to develop tools, rooted in the health-environment data community, aiming at addressing shared issues.

Link : 
<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Website</a>

## API2Build'

Air quality has an impact on people's health. Indoor air quality (IAQ) and outdoor air quality (OAQ) data are produced by different stakeholders, on different geographical and temporal scales, and are therefore not linked, limiting the interpretations that can be made of IAQ measurements. Reconciling these data would make it possible to assess the relationship between IAQ and EQA.

<a href="https://gd4h.ecologie.gouv.fr/defis/732" target="_blank" rel="noreferrer">Find out more about the challenge</a>

## **Documentation**

**Solution description:**
Automated retrieval of open-source OAQ data for spatial and temporal matching with IAQ data, thanks to 2 Python algorithms for retrieving and matching OAQ data to a sample of geolocated buildings, over the desired period per building.

2 sources of OAQ data :
- Source 1: observed OAQ data, retrieved from geodair.fr
- Source 2: modeled OAQ data, supplied by INERIS

Implementation steps :
- Data mining and qualification
- Programming of algorithms for recovering OAQ data and matching them to building data
- Program testing
- Identification of areas for improvement

### **Installation**

[Installation Guide](/INSTALL.md)

### **Usage**

The **notebooks** folder contains the data matching scripts for Géodair and Chimère.

**Output:**
- Géod'air: obtain an output file containing, for a selected CSTB building, its position, the 10 closest Geod'air OAQ measurement stations, the OAQ measurements for each of the selected pollutants over the survey period for which IAQ measurements are available.

- Chimère: obtain an .xlsx file containing the identifiers and coordinates of the different buildings, as well as the different measurement campaign dates and the average, median, minimum and maximum concentrations of the different pollutants for the corresponding measurement campaign dates.

### **Perpetuation and lines of development**

**Challenges to perpetuation**
- Assessment of building occupants' exposure to outdoor pollution
- Use of solutions developed as part of studies on indoor and outdoor air quality
  (e.g. pollution transfer, IAQ predictive models based on EQA data)

**Lines of development**
- Improvement of the data matching method through the implementation of more refined matching rules (e.g. restriction on distances between OAQ measuring stations and matched buildings)
- Optimization of developed algorithms

### **Contributing**

If you wish to conribute to this project, please follow the [recommendations](/CONTRIBUTING.md).

### **Licence**

The code is published under [MIT Licence](/LICENSE).

The data referenced in this README and in the installation guide is published under <a href="https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf">Open Licence 2.0</a>.
