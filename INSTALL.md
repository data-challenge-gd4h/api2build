# Installation Guide

## Data Collection

- **Liste des bâtiments du CSTB :** contacter le <a href="https://www.cstb.fr/fr/" target="_blank" rel="noreferrer">CSTB</a> pour l'obtenir
  Intégrer ces données dans un dossier data/cstb

- **Liste des stations Geod'air :** demander une clé API à Geod'air puis se connecter à l'API "Stations" : <a href="https://www.geodair.fr/donnees/api" target="_blank" rel="noreferrer">Geod'air API</a>
  Cf. section "Dependencies" pour le chargement de cette clé d'API.

- **Historique des polluants Geod'air :** télécharger l'historique des polluants, par période de 6 mois (par souci de limite de taille de fichier) : <a href="https://www.geodair.fr/donnees/export-advanced" target="_blank" rel="noreferrer">Geod'air Export données polluants</a>
  Intégrer ces données dans un dossier data/geodair/[nom polluant]
  *Ex : data/geodair/NO2*

- **Liste des données Chimère :** contacter le <a href="https://www.cstb.fr/fr/" target="_blank" rel="noreferrer">CSTB</a> pour l'obtenir
  Intégrer ces données dans un dossier data/chimere

## Dependencies

- Utiliser le langage Python

- Créer un environnement virtuel avec virtualenv et installer les modules du `requirements.txt`:

  ```bash
  python -m virtualenv .venv/
  source .venv/bin/activate
  pip install -r requirements.txt
  ```

  Pour sortir de l'environnement virtuel :

  ```bash
  deactivate
  ```

- Charger des variables locales dans l'environnement :

  1 - Créer un fichier `.env` et y ajouter la clé d'API sous ce format :

  ```python
  # API Key Geod'Air Louis
  apikey_geodair='XXXXXXXXX'
  ```

  2 - Créer un fichier `.envrc`et y ajouter le code suivant :

  ```python
  dotenv
  ````

  3 - Dans le CLI, lancer la commande suivante :

  ```bash
  direnv allow .
  ```

  Pour recharger les variables d'environnement après une modification :

  ```bash
  direnv reload
  ```

## Exécution

cf. [issue](https://gitlab.com/data-challenge-gd4h/api2build/-/issues/2).
