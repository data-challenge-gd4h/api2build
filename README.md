# Challenge GD4H - API2Build'

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Dans ce cadre, un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées a été organisé en 2023.

<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a> 

## API2Build'

La qualité de l’air a un impact sur la santé des individus. Les données de qualité de l’air intérieur (QAI) et de l’air extérieur (QAE) sont produites par des acteurs différents, sur différentes échelles géographiques et temporelles, et ne sont donc pas mises en lien, limitant ainsi les interprétations qui peuvent être faites des mesures de QAI. Le rapprochement de ces données permettrait d’évaluer les relations entre la QAI et la QAE.

<a href="https://gd4h.ecologie.gouv.fr/defis/732" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Documentation**

**Description solution :**
récupérer de façon automatisée les données de la QAE en open source pour un appariement spatial et temporel avec les données de la QAI, grâce à 2 algorithmes Python de récupération et d’appariement des données de la QAE à un échantillon de bâtiments géolocalisés, sur la période souhaitée par bâtiment.

2 sources de données de la QAE :
- Source 1 : données observées de la QAE, récupérées sur geodair.fr
- Source 2 : données modélisées de la QAE, fournies par l’INERIS

Étapes de mise en œuvre :
- Exploration et qualification des données
- Programmation des algorithmes de récupération des données de la QAE et de leur appariement aux données bâtiments
- Tests des programmes
- Identification des axes d’amélioration

### **Installation**

Pour la suite des installations : [Guide d'installation](/INSTALL.md)

### **Utilisation**

Dans le dossier **notebooks**, figurent les scripts d'appariement des données pour Géodair et Chimère.

**Output :**
- Géod'air :  obtention d'un fichier de sortie contenant, pour un bâtiment du CSTB sélectionné, sa position, les 10 stations de mesures de la QAE Geod'air les plus proches, les mesures de QAE pour chacun des polluants sélectionnés sur la période d'enquête pour laquelle les mesures de QAI sont disponibles

- Chimère : obtention d'un fichier .xlsx contenant les identifiants et coordonées de différent batiments, ainsi que les différentes dates de campagne de mesures et les concentrations moyennes, médianes, minimales et maximales des différents poluant pour les dates de comapgne de mesures conrespondantes.

### **Pérénnisation et axes de poursuite**

**Enjeux de pérennisation**
- Évaluation de l’exposition des occupants des bâtiments à la pollution de l’environnement extérieur
- Utilisation des solutions développées dans le cadre d’études sur la qualité de l’air intérieur et extérieur
  (ex : transfert de pollution, modèles prédictifs de la QAI à partir de données de la QAE)

**Axes de poursuite du défi envisagés**
- Amélioration de la méthode d’appariement des données par la mise en place de règles plus fines d’appariement
  (ex : restriction sur les distances entre les stations de mesure de la QAE et les bâtiments appariés)
- Optimisation des algorithmes développés

### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).
